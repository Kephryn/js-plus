/*
JSPlus: XHR+ (XmlHttpRequests-Plus)

Copyright (c) 2015 Yvon Viger

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

//http://pixelsvsbytes.com/blog/2011/12/teach-your-xmlhttprequest-some-json/
//https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/Using_XMLHttpRequest
//https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/Sending_and_Receiving_Binary_Data

//https://github.com/jDataView/jBinary
//https://developer.mozilla.org/en/docs/Web/API/XMLHttpRequest
/*
	List features I would like:
	USE req.overrideMimeType('text\/plain; charset=x-user-defined'); to get Binary from old browsers


	var data = xhr('POST', '/?erism=true', 'json', 'json')
		.callback(function(err, response){
			console.log(response);
		})
		.send({
			test: 'data',
			fun: 'stuff'
		});
*/

(function() {
	/**
	 * @param {String} m=GET - Method
	 * @param {String} l - URL
	 * @param {String} [t=urlencoded - Request Encoding
	 * @param {String} [r] - Response Data Type
	 */
	var xhr = function(m, l, t, r) {
		var xhr = new XMLHttpRequest();

		if('responseType' in xhr && 'response' in xhr && 'withCredentials' in xhr)
			return new XMLHttpRequestPlus(xhr,m, l, t, r);
		else
			throw new Error('Your browsers implementation of XMLHttpRequest is not yet supported. SoL');
	}

	/**
	 * XMLHttpRequestPlus for fast and easy XHR requests, keep things light and easy to manipulate
	 * @constructor
	 * @param {XMLHttpRequest} xhr - XMLHttpRequest Object
	 * @param {String} method=GET - Method
	 * @param {String} location - URL
	 * @param {String} [requestEncoding=urlencoded] - Request Encoding
	 * @param {String} [responseType] - Response Data Type
	 * @returns {XMLHttpRequestPlus}
	 */
	var XMLHttpRequestPlus = function(xhr, method, location, requestEncoding, responseType) {
		var self = this,
			err = null,
			res;

		this.conn = xhr;
		this.method = method || 'GET';
		this.location = location || '/';
		this.requestEncoding = requestEncoding || 'urlencoded';
		this.responseType = responseType || '';
		this.headers = {};
		this.username = '';
		this.password = '';
		this.jsonp = false;

		_(this.conn).on('error', function(e) {
			e.preventDefault();
			console.log(e);
			return false;
		});

		// _(this.conn).on('load', function(e) {
		// 	console.log(e);
		// });

		_(this.conn).on('abort', function(e) {
			console.log(e);
		});

		_(this.conn).on('readystatechange', function(e) {
			if (self.conn.readyState == 4) {
				if(self.conn.status !== 200)
					err = 'This may not be an error. This code needs to be expanded... Not status 200. You got: ' + self.conn.status;

				res = self.conn.response || self.conn.responseXML || self.conn.responseText;

				if(self.callback)
					self.callback.call(self, err, res, self.getHeaders());
			}
		});

		this.timeout = setTimeout(function() {
			self._send.call(self);
		});

		return this;
	}


	/**
	 * Method for setting headers to XHR
	 * @param {Object} headers - Set XHR headers
	 * @returns {XMLHttpRequestPlus}
	 */
	XMLHttpRequestPlus.prototype.setHeaders = function(headers) {
		var self = this;
		if(this.timeout) clearTimeout(this.timeout);

		for(prop in headers) {
			if(headers.hasOwnProperty(prop)) {
				console.log(prop, headers[prop]);
				this.headers[prop] = headers[prop];
			}
		}
		this.timeout = setTimeout(function() {
			self._send.call(self);
		});
		return this;
	}

	/**
	 * Method for getting headers from XHR
	 * @returns {Object} headers
	 */
	XMLHttpRequestPlus.prototype.getHeaders = function() {
		var self = this,
			headers = {},
			pairs, key, val,
			lines = this.conn.getAllResponseHeaders().split('\n');

		for(var i = 0; i < lines.length; i++) {
			pairs = lines[i].split(':');
			if(pairs.length == 2) {
				key = pairs[0];
				val = pairs[1];
				headers[key.trim()] = val.trim();
			}
		}
		return headers;
	}

	/**
	 * Set XHR Data, method has two names, or a name an alias, you choose
	 * @param {Object} data - Key value pairs that are compiled based on request type
	 * @returns {XMLHttpRequestPlus}
	 */
	XMLHttpRequestPlus.prototype.data =
	XMLHttpRequestPlus.prototype.payload =
	function(data) {
		var self = this;
		if(this.timeout) clearTimeout(this.timeout);

		if(this.requestEncoding == 'plain') {
			var pairs = [];
			this.headers['content-type'] = 'text/plain';
			for(prop in data) {
				pairs.push([prop, data[prop]].join('='));
			}

			this.data = pairs.join('\n');
		}
		else if(this.requestEncoding == 'multipart') {
			this.data = new FormData();

			for(prop in data) {
				this.data.append(prop, data[prop]);
			}
		}
		else if(this.requestEncoding == 'urlencoded') {
			var pairs = [];
			this.headers['content-type'] = 'application/x-www-form-urlencoded';

			for(prop in data) {
				pairs.push([prop, encodeURIComponent(data[prop])].join('='));
			}

			this.data = pairs.join('&');
		}
		else if(this.requestEncoding == 'json') {
			this.headers['content-type'] = 'application/json;charset=UTF-8';
			this.data = JSON.stringify(data);
		}
		else {
			throw new Error('Encoding Type is not defined.');
		}

		this.timeout = setTimeout(function() {
			self._send.call(self);
		});

		return this;
	}

	/**
	 * Set the Callback method
	 * @param {Function} callback - Callback method that will be invoked after the XHR
	 * @returns {XMLHttpRequestPlus}
	 */
	XMLHttpRequestPlus.prototype.callback = function(callback) {
		var self = this;

		if(this.timeout) clearTimeout(this.timeout);

		this.callback = callback;
		this.timeout = setTimeout(function() {
			self._send.call(self);
		});
		return this;
	}

	/**
	 * Set the progress handler methods
	 * @param {Function} progress - Callback method that will be invoked after progress state change
	 * @returns {XMLHttpRequestPlus}
	 */
	XMLHttpRequestPlus.prototype.progress = function(progress) {
		var self = this;

		if(this.timeout) clearTimeout(this.timeout);

		_(this.conn).on('progress', progress);

		this.timeout = setTimeout(function() {
			self._send.call(self);
		});
		return this;
	}

	/**
	 * Auth and Authenticate for XHR, it is possible to omit this method and have the browser prompt for these values
	 * @param {String} username - The username
	 * @param {String} password - The password
	 * @returns {XMLHttpRequestPlus}
	 */
	XMLHttpRequestPlus.prototype.auth =
	XMLHttpRequestPlus.prototype.authenticate =
	function(username, password) {
		var self = this;

		if(this.timeout) clearTimeout(this.timeout);

		this.username = username;
		this.password = password;
		this.timeout = setTimeout(function() {
			self._send.call(self);
		});
		return this;
	}

	/**
	 * Internal workhorse, does all the heavy lifting, sending etc.
	 * @param {String} username - The username
	 * @returns {XMLHttpRequestPlus}
	 */
	XMLHttpRequestPlus.prototype._send = function(data) {
		var self = this,
			err;

		if(this.timeout) clearTimeout(this.timeout);
		if(data !== undefined) {
			this.data(data);

			if(this.method.toUpperCase() === 'GET') {
				this.location += ((this.location.indexOf('?') != -1) ? '&' : '?') + this.data;
			}
		}

		if(self.jsonp) {
			var script = document.createElement('script');
			script.onload = function() {
				if(self.callback)
					self.callback.call(self, err, res, this.conn.getAllResponseHeaders());
			};
			script.src = this.location;
			document.head.appendChild(script);
		}
		else {
			this.conn.open(this.method.toUpperCase(), this.location, true, this.username, this.password);
			this.conn.responseType = this.responseType;

			for(prop in this.headers) {
				this.conn.setRequestHeader(prop, this.headers[prop]);
			}

			try {
				this.conn.send(this.data);
			}
			catch(exception) {
				console.log(exception);
				if(exception.name == 'NetworkError') {
					console.log('There was a network error.');
				}
			}
		}

		return this;
	}

	window.xhr = xhr;
}());



